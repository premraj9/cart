<?php
$documentRoot = $_SERVER['DOCUMENT_ROOT'];
include_once $documentRoot . '/model/Cart.php';

$action = $_REQUEST['action'];
$value = $_REQUEST['value'];

// controll the functions accoring to the actions requested
$cart = new Cart();

// check the request action is not empty
if (!empty($action)) {
    // add item to the list
    if ($action === 'addItem') {
        $cart->addItem($value);
    } else if ($action === 'removeItem') {
        $cart->removeItem($value);
    } else if ($action === 'calculateTotal') {
        $cart->calculateTotal($value);
    }
}