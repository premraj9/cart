<?php

/**
 * Class Cart
 *
 * Contains functionality for keeping control of the items in the cart
 */
class Cart
{
    /**
     * local session storage container
     * 
     * @var array
     */
    public $store;

    /**
     * Cart constructor.
     *
     * Starts the session and create store
     */
    public function __construct()
    {
        session_start();
        // if session is not empty create session store
        if (!isset($_SESSION['store'])) {
            $_SESSION['store'] = [];
        }
        
        $_COOKIE['store'] = [];
    }
    
    /**
     * set the new store
     */
    public function setStore() 
    {
        $this->store = $_SESSION['store'];
    }

    /**
     * Initializes cart if it has not been set yet
     *
     * @param array $data - array of item data
     */
    public function InitialItems($data)
    {
        // $this->setStore();
        if (!isset($_SESSION['store'])) {
            $_SESSION['store'] = [];
        }
        
        $rowData = [
            'name' => $data[0],
            'price' => $data[1],
            'quantity' => 0,
            'total' => 0,
        ];
        // init item
        if (!isset($_SESSION['store'][$data[0]])) {
            $_SESSION['store'][$data[0]] = $rowData;
        }        
    }

    /**
     * Add selected item to the cart
     *
     * @param string $value - item  data in Json format
     */
    public function addItem($value)
    {
        // $this->setStore();
        $data = explode(':', $value);
        // init cart
        $this->InitialItems($data);
        $quantity =  $_SESSION['store'][$data[0]]['quantity'] + 1;
        $totalInt = $quantity * $data[1];
        $total = number_format(floatval($totalInt), 2, '.', '');

        $_SESSION['store'][$data[0]]['quantity'] = $quantity;
        $_SESSION['store'][$data[0]]['total'] = $total;
                
        print_r(json_encode($_SESSION['store']));
    }

    /**
     * Remove the selected item from the cart list
     *
     * @param string $value - date of the item to remove from cart
     */
    public function removeItem($value)
    {
        $this->setStore();
        $data = explode(':', $value);
        $quantity = $_SESSION['store'][$data[0]]['quantity'] - 1;
        $totalInt = $quantity * $data[1];
        $total = number_format(floatval($totalInt), 2, '.', '');
        if ($quantity == 0) {
            unset($_SESSION['store'][$data[0]]);
            print_r(json_encode($_SESSION['store']));
            die();
        }
        $_SESSION['store'][$data[0]]['quantity'] = $quantity;
        $_SESSION['store'][$data[0]]['total'] = $total;
        print_r(json_encode($_SESSION['store']));
    }

    /**
     * Calculates the items in cart to know the total
     */
    public function calculateTotal()
    {
        $this->setStore();
        $total = 0;
        if (isset($_SESSION['store']) && !empty($_SESSION['store'])) {
            foreach ($_SESSION['store'] as $value) {
                $itemTotal = $value['total'];
                $total += $itemTotal;       
            }
        }
        
        $roundTotal = round($total, 2);
        print_r(json_encode(['total' => $roundTotal]));
    }
}
