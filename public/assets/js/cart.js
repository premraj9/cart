    /**
     * Builds rows of cart table content
     *
     * @param { string } data - json object of a single cart row data
     *
     * @returns {string|string}
     */
    function addNewRow(data) {
        var value = data.name + ":" + data.price;
        var row = "<tr>";
        row = row + "<td>" + data.name + "</td>";
        row = row + "<td>" + data.price + "</td>";
        row = row + "<td>" + data.quantity + "</td>";
        row = row + "<td>" + data.total + "</td>";
        row = row + "<td><button class='remove' value='" + value + "'>Remove</button></td>";
        row = row + "</tr>";
        return row;
    };
    /**
     * Redraws the cart table
     *
     * @param {string} data - complete json object of cart table data
     */
    function orderRaw(data) {
        var store = $.parseJSON(data);
        // var cart = session.store;
        var table = "";
        $.each(store, function (index, value) {
            table += addNewRow(value);
        });
        $("#cart-items").html(table);
        $(".remove").unbind();
        $(".remove").bind("click", function () {
            var value = $(this).val();
            var data = {
                action: 'removeItem',
                value
            };
            $.ajax({
                url: 'controller/Cart.php',
                type: 'GET',
                data,
                success: function (data) {
                    orderRaw(data);
                }
            });
        });
    };
    
    /**
     * Calculate and displays cart total
     */
    function calculateTotal() {
        var data = {
            action : 'calculateTotal'
        };
        $.ajax({
            url: 'controller/Cart.php',
            type: 'GET',
            data,
            success: function (data) {
                var totalData = $.parseJSON(data);
                var total = "<tr>";
                total = total + "<td colspan='3'>Total</td>";
                total = total + "<td>" + totalData.total + "</td>";
                total = total + "<td></td>";
                total = total + "</tr>";
                $("#cart-items").append(total);
            }
        });
    };

    $(document).ready(function () {
        $(".add").click(function (e) {
            var value = $(this).val();
            var data = {
                action: 'addItem',
                value
            };
            $.ajax({
                url: 'controller/Cart.php',
                type: 'GET',
                data,
                success: function (data) {
                    orderRaw(data);
                }
            });
        });
        
        $(".remove").bind("click", function () {
            var value = $(this).val();
            var data = {
                action: 'removeItem',
                value
            };
            $.ajax({
                url: 'controller/Cart.php',
                type: 'GET',
                data,
                success: function (data) {
                    orderRaw(data);
                }
            });
        });
    });