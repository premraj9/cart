<?php
$documentRoot = $_SERVER['DOCUMENT_ROOT'];
include_once $documentRoot . '/controller/Cart.php';

$products = [
    ['name' => 'Sledgehammer', 'price' => 125.75],
    ['name' => 'Axe', 'price' => 190.5],
    ['name' => 'Bandsaw', 'price' => 562.13],
    ['name' => 'Chisel', 'price' => 12.9],
    ['name' => 'Hacksaw', 'price' => 18.45],
];

$cart = new Cart();

?>

<div class="col-md-12 shopping-list">
    <table>
        <tr>
            <th style="width:30%">#</th>
            <th style="width:30%">Name</th>
            <th style="width:30%">Price</th>
        </tr>
<?php foreach ($products as $item) { ?>
            <tr>
                <td>
                    <button class="add" value="<?php echo $item['name'] . ':' . $item['price'] ?>">
                        Add
                    </button>
                </td>
                <td><?php echo $item['name'] ?></td>
                <td><?php echo $item['price'] ?></td>
            </tr>
<?php } ?>
    </table>
</div>
<div class="col-md-12 cart-container">
    <table class="cart-table">
        <tr>
            <th colspan="5">Your cart</th>
        </tr>
        <tr>
            <th style="width:20%">Name</th>
            <th style="width:10%">Price</th>
            <th style="width:10%">Amount</th>
            <th style="width:10%">Total</th>
            <th style="width:20%"></th>            
        </tr>
        <tbody id="cart-items">
            <?php if (isset($_SESSION['store']) && !empty($_SESSION['store'])) { ?>
    <?php foreach ($_SESSION['store'] as $key => $data) { ?>
                    <tr>
                        <td><?php echo $data['name'] ?></td>
                        <td><?php echo $data['price'] ?></td>
                        <td><?php echo $data['quantity'] ?></td>
                        <td><?php echo $data['total'] ?></td>
                        <td>
                            <button class="remove" value="<?php echo $data['name'] . ':' . $data['price'] ?>">
                                Remove
                            </button>
                        </td>
                    </tr>
                <?php } ?>
<?php } ?>
        </tbody>
    </table>
</div>

